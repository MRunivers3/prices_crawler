<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="https://secure.gravatar.com/avatar/7898c0559c80b40b93ef1b25446272d4?r=g&d=https://avatar-cdn.atlassian.com/default/96&s=64">
    <title>Search it</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89936112-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-89936112-1');
    </script>
</head>
<body>
<label for="item_type">What are you searching for in insomnia dell section?</label>
<input id="item_type" type="text" name="name" title="Item name">
</body>
<script src="/resources/js/vue.js"></script>
<script type="text/javascript" src="/resources/js/home.js"></script>
</html>