<?php

namespace App\Traits;

/**
 * Trait ArrayMerge
 * @package App\Traits
 */
trait ArrayMerge
{
    /**
     * Function for merging arrays with same key values
     *
     * @param array $array1
     * @param array $array2
     *
     * @return array
     */
    static public function mergePerKey($array1, $array2)
    {
        // Init merged array
        $mergedArray = [];

        foreach ($array1 as $key => $value) {
            try {
                // Merge only if key exists
                $mergedArray[$value] = $array2[$key];
            } catch (\Exception $ex) {
                // Do nothing
            }
        }

        // Return merged array
        return $mergedArray;
    }
}