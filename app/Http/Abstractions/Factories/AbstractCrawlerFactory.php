<?php

namespace App\Http\Abstractions\Factories;

/**
 * Class AbstractCrawlerFactory
 * @package App\Http\Abstractions\Factories
 */
abstract class AbstractCrawlerFactory
{
    /**
     * @param string $siteTitle
     *
     * @return mixed
     */
    abstract function createCrawler($siteTitle);
}