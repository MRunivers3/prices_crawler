<?php

namespace App\Http\Controllers;

use App\Http\Abstractions\AbstractCrawlData;
use App\Http\Abstractions\Factories\AbstractCrawlerFactory;
use App\Providers\Services\Insomnia;
use http\Exception\InvalidArgumentException;
use Laravel\Lumen\Application;

/**
 * Class CrawlerFactory
 * @package App\Http\Controllers
 */
class CrawlerFactory extends AbstractCrawlerFactory
{
    const INSOMNIA = 'insomnia';
    const EBAY = 'ebay';
    const FACEBOOK = 'facebook';
    const XE = 'xe';

    /**
     * Get crawler based on selected site
     *
     * @param string $title
     *
     * @return AbstractCrawlData
     * @throws InvalidArgumentException
     */
    function createCrawler($title)
    {
        switch ($title) {
            case self::INSOMNIA:
                return Application::getInstance()->make(Insomnia::class);
                break;
            default:
                throw new InvalidArgumentException('The category you asked does not exist');
        }
    }
}