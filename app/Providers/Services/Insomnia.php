<?php
/**
 * Created by PhpStorm.
 * User: Xps
 * Date: 03-Dec-18
 * Time: 9:52 PM
 */

namespace App\Providers\Services;

use App\Http\Abstractions\AbstractCrawlData;
use App\Traits\ArrayMerge;
use Faker\Generator;
use Faker\Provider\UserAgent;
use Goutte\Client;

/**
 * Class Insomnia
 * @package App\Providers\Services
 */
class Insomnia extends AbstractCrawlData
{
    use ArrayMerge;

    const INSOMNIA_CLASS_MAX_PAGES = '.ipsPagination_last a';
    const INSOMNIA_CLASS_ITEM_TITLE = '.ipsTruncate_line';
    const INSOMNIA_CLASS_ITEM_DESCRIPTION = 'unknown';
    const INSOMNIA_CLASS_ITEM_PRICE = '.cFilePrice .ipsType_normal';

    /**
     * @var UserAgent
     */
    public $userAgent;

    /**
     * @var array
     */
    private $names = [];

    /**
     * @var array
     */
    private $prices = [];

    /**
     * @return array
     */
    public function crawlInsomnia()
    {
        // Create random user agent generator for crawler
        $userAgent = (new UserAgent(new Generator()))->userAgent();

        // Create new client
        $client = new Client();

        // Crawl certain url
        $i = 0;
        $client->setHeader('User-Agent', $userAgent);
        $client->setHeader('allow_redirects', false);

        // Get all pages number
        try {
            $crawler = $client->request('GET', 'https://www.insomnia.gr/classifieds/category/56-dell');

            $totalPages = $crawler->filter(self::INSOMNIA_CLASS_MAX_PAGES)->getNode(0)->getAttribute('data-page');
        } catch (\Exception $ex) {
            // Do nothing for now
            $totalPages = -10;
        }

        // Check the rest of the pages
        $pages = [];
        while ($i++ <= $totalPages) {
            $pages[] = $client->request('GET', sprintf('https://www.insomnia.gr/classifieds/category/56-dell/?page=%s', $i));
        }

        foreach ($pages as $page) {
            $page->filter(self::INSOMNIA_CLASS_ITEM_TITLE)->each(function ($node) {
                array_push($this->names, $node->text());
            });

            $page->filter(self::INSOMNIA_CLASS_ITEM_PRICE)->each(function ($node) {
                array_push($this->prices, $node->text() ?: 'No price');
            });
        }

        // Display results
        return ArrayMerge::mergePerKey($this->names, $this->prices);
//        $count = 1;
//        foreach ($results as $key => $result) {
//            echo sprintf('%s) <span>%s</span>  -- <b>%s</b><br>', $count++, $key, $result);
//        }
    }
}