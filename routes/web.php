<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Group routes with api prefix
//$router->group(['prefix' => 'api'], function ($router) {
//    $router->group(['prefix' => 'crawl'], function ($router) {
//        $router->get('/insomnia', 'InsomniaController@get');
//        $router->get('/info', 'InfoController@displayInfo');
//    });
//});

$router->get('/{route:.*}/', function () {
    return view('app');
});